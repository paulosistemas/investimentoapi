package br.com.investimento.services;

import br.com.investimento.models.Investimento;
import br.com.investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService{

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento){
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Iterable<Investimento> buscarTodos(){
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    public void deletarInvestimento(int id) {
        if (investimentoRepository.existsById(id)) {
            investimentoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Lead não foi encontrado");
        }
    }

    public Investimento atualizarInvestimento(int id, Investimento investimento){
        if (investimentoRepository.existsById(id)){
            investimento.setId(id);
            Investimento investimentoObjeto = salvarInvestimento(investimento);
            return investimentoObjeto;
        }
        throw new RuntimeException("Investimento não foi encontrado");
    }

    public Investimento buscarPorId(int id){
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        if (optionalInvestimento.isPresent()){
            return optionalInvestimento.get();
        }
        throw new RuntimeException("O investimento não foi encontrado");
    }
}


//@Service
//public class LeadService {
//
//    @Autowired
//    private LeadRepository leadRepository;
//
//    public Lead salvarLead(Lead lead){
//        Lead leadObjeto = leadRepository.save(lead);
//        return leadObjeto;
//    }
//
//    public Iterable<Lead> buscarTodos(){
//        Iterable<Lead> leads = leadRepository.findAll();
//        return leads;
//    }
//
//
//    public Iterable<Lead> buscarTodosPorTipoLead(TipoLeadEnum leadEnum){
//        Iterable<Lead> leads = leadRepository.findAllByTipoLead(leadEnum);
//        return leads;
//    }
//
//    public Lead buscarPorId(int id){
//        Optional<Lead> optionalLead = leadRepository.findById(id);
//        if (optionalLead.isPresent()){
//            return optionalLead.get();
//        }
//        throw new RuntimeException("O lead não foi encontrado");
//    }
//

//
//    public void deletarLead(int id){
//        if (leadRepository.existsById(id)){
//            leadRepository.deleteById(id);
//        }else{
//            throw new RuntimeException("Lead não foi encontrado");
//        }
//    }