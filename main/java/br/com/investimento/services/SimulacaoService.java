package br.com.investimento.services;

import br.com.investimento.DTOs.RespostaDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.InvestimentoRepository;
import br.com.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Simulacao salvarSimulacao (Simulacao simulacao, int idInvestimento){
        Optional<Investimento> optionalInvestimentoDaSimulacao = investimentoRepository.findById(idInvestimento);

        if (optionalInvestimentoDaSimulacao.isPresent()){
            simulacao.setInvestimentos(optionalInvestimentoDaSimulacao.get());
            Simulacao simulacaoObjeto = simulacaoRepository.save(simulacao);
            return simulacaoObjeto;
        }
        throw new RuntimeException("O investimento não foi encontrado");
    }

    public RespostaDTO calcularRendimento(Simulacao simulacao){
        RespostaDTO respostaDTO = new RespostaDTO();
//        double capitalInicial = simulacao.getValorAplicado();
        int quantidadeMeses = simulacao.getQuantidadeMeses();
        double taxaRendimento = simulacao.getInvestimento().getTaxaRendimento();
        double montante = simulacao.getValorAplicado();

        for (int i=0; i<quantidadeMeses;i++){
            montante *= (1 + taxaRendimento);
        }
        respostaDTO.setMontante(montante);
        respostaDTO.setRendimentoPorMes(taxaRendimento*100);
        return respostaDTO;
    }

    public Iterable<Simulacao> buscarTodos(){
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        return simulacoes;
    }
}


//    @Autowired
//    private InvestimentoRepository investimentoRepository;
//
//    public Investimento salvarInvestimento(Investimento investimento){
//        Investimento investimentoObjeto = investimentoRepository.save(investimento);
//        return investimentoObjeto;
//    }
//
//    public Iterable<Investimento> buscarTodos(){
//        Iterable<Investimento> investimentos = investimentoRepository.findAll();
//        return investimentos;
//    }
//
//    public void deletarInvestimento(int id) {
//        if (investimentoRepository.existsById(id)) {
//            investimentoRepository.deleteById(id);
//        } else {
//            throw new RuntimeException("Lead não foi encontrado");
//        }
//    }
//
//    public Investimento atualizarInvestimento(int id, Investimento investimento){
//        if (investimentoRepository.existsById(id)){
//            investimento.setId(id);
//            Investimento investimentoObjeto = salvarInvestimento(investimento);
//            return investimentoObjeto;
//        }
//        throw new RuntimeException("Lead não foi encontrado");
//    }
//
//    public Investimento buscarPorId(int id){
//        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
//        if (optionalInvestimento.isPresent()){
//            return optionalInvestimento.get();
//        }
//        throw new RuntimeException("O lead não foi encontrado");
//    }