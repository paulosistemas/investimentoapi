package br.com.investimento.controllers;

import br.com.investimento.DTOs.RespostaDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.services.InvestimentoService;
import br.com.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> exibirTodos(){
        Iterable<Simulacao> simulacoes = simulacaoService.buscarTodos();
        return simulacoes;
    }
}


//public class MatematicaController {
//
//    @Autowired
//    private MatematicaService matematicaService;
//
//    @PutMapping("/soma")
//    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO) {
//        if(entradaDTO.getNumeros().size() <= 1){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros para serem somados");
//        }
//        RespostaDTO resposta = matematicaService.soma(entradaDTO);
//        return resposta;
//    }
//}


//@RestController
//@RequestMapping("/investimento")
//public class InvestimentoController {
//
//    @Autowired
//    private InvestimentoService investimentoService;
//
//    @PostMapping
//    public ResponseEntity<Investimento> registarInvestimento(@RequestBody @Valid Investimento investimento) {
//        Investimento leadObjeto = investimentoService.salvarInvestimento(investimento);
//        return ResponseEntity.status(201).body(investimento);
//    }
//
//    @GetMapping
//    public Iterable<Investimento> exibirTodos(){
//        Iterable<Investimento> investimentos = investimentoService.buscarTodos();
//        return investimentos;
//    }
//
//    @DeleteMapping("/{id}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void deletarInvestimento(@PathVariable int id){
//        try{
//            investimentoService.deletarInvestimento(id);
//        }catch (RuntimeException exception){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
//        }
//    }
//
//    @PutMapping("/{id}")
//    public Investimento atualizarInvestimento(@PathVariable(name = "id") int id,
//                                              @RequestBody Investimento investimento){
//        try{
//            Investimento investimentoObjeto = investimentoService.atualizarInvestimento(id, investimento);
//            return investimentoObjeto;
//        }catch (RuntimeException exception){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
//        }
//    }
//
//    @GetMapping("/{id}")
//    public Investimento bucarPorId(@PathVariable(name = "id") int id){
//        try{
//            Investimento investimento = investimentoService.buscarPorId(id);
//            return investimento;
//        }catch (RuntimeException exception){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
//        }
//    }