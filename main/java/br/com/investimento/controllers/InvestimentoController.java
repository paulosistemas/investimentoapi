package br.com.investimento.controllers;

import br.com.investimento.DTOs.RespostaDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.services.InvestimentoService;
import br.com.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping("/{idInvestimento}/simulacao")
    public ResponseEntity<RespostaDTO> registarSimulacao(@RequestBody @Valid Simulacao simulacao,
                                                         @PathVariable int idInvestimento) {
        Simulacao simulacaoObjeto = simulacaoService.salvarSimulacao(simulacao, idInvestimento);
        RespostaDTO resposta = simulacaoService.calcularRendimento(simulacao);
        return ResponseEntity.status(201).body(resposta);
    }

    @PostMapping
    public ResponseEntity<Investimento> registrarInvestimento(@RequestBody @Valid Investimento investimento) {
        Investimento leadObjeto = investimentoService.salvarInvestimento(investimento);
        return ResponseEntity.status(201).body(investimento);
    }

    @GetMapping
    public Iterable<Investimento> exibirTodos(){
        Iterable<Investimento> investimentos = investimentoService.buscarTodos();
        return investimentos;
    }

      @DeleteMapping("/{id}")
      @ResponseStatus(HttpStatus.NO_CONTENT)
      public void deletarInvestimento(@PathVariable int id){
          try{
              investimentoService.deletarInvestimento(id);
          }catch (RuntimeException exception){
              throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
          }
      }

        @PutMapping("/{id}")
        public Investimento atualizarInvestimento(@PathVariable(name = "id") int id,
                                                  @RequestBody Investimento investimento){
            try{
                Investimento investimentoObjeto = investimentoService.atualizarInvestimento(id, investimento);
                return investimentoObjeto;
            }catch (RuntimeException exception){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
            }
        }

        @GetMapping("/{id}")
        public Investimento bucarPorId(@PathVariable(name = "id") int id){
            try{
                Investimento investimento = investimentoService.buscarPorId(id);
                return investimento;
            }catch (RuntimeException exception){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
            }
        }
}






