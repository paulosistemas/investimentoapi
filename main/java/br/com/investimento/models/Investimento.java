package br.com.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Investimento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 5, max = 40, message = "Nome deve ter entre 5 a 40 caracteres")
    @NotBlank(message = "O campo nome deve ser preenchido")
    @Column(unique = true)
    private String nomeInvestimento;

    @NotNull(message = "Taxa não pode ser nula")
    @Digits(integer = 12, fraction = 2, message = "Taxa deve ter 2 casas decimais")
    private double taxaRendimento;

    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeInvestimento() {
        return nomeInvestimento;
    }

    public void setNomeInvestimento(String nomeInvestimento) {
        this.nomeInvestimento = nomeInvestimento;
    }

    public double getTaxaRendimento() {
        return taxaRendimento;
    }

    public void setTaxaRendimento(double taxaRendimento) {
        this.taxaRendimento = taxaRendimento;
    }
}
