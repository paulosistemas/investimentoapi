package br.com.investimento.models;

import net.bytebuddy.dynamic.loading.InjectionClassLoader;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSimulacao;

    @Size(min = 5, max = 40, message = "Nome deve ter entre 5 a 40 caracteres")
    @NotBlank(message = "O campo nome deve ser preenchido")
    private String nomeInteressado;

    @Email(message = "Formato do e-mail inválido")
    @NotNull(message = "Email não pode ser nulo")
    private String emailInteressado;

    @NotNull(message = "Valor Aplicado não pode ser nulo")
    @Digits(integer = 12, fraction = 2, message = "Valor Aplicado deve ter 2 casas decimais")
    private double valorAplicado;

    @Min(value = 1)
    @NotNull
    private int quantidadeMeses;

// 1 simulação terá um investimento e 1 investimento poderá estar em várias simulações. FK deverá estar na simulação
    @ManyToOne(cascade = CascadeType.ALL)
    private Investimento investimento;

    public Simulacao() {
    }

    public int getIdSimulacao() {
        return idSimulacao;
    }

    public void setIdSimulacao(int idSimulacao) {
        this.idSimulacao = idSimulacao;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmailInteressado() {
        return emailInteressado;
    }

    public void setEmailInteressado(String emailInteressado) {
        this.emailInteressado = emailInteressado;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimentos(Investimento investimento) {
        this.investimento = investimento;
    }
}


//@Entity
//public class Investimento {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private int id;
//
//    @Size(min = 5, max = 40, message = "Nome deve ter entre 5 a 40 caracteres")
//    @NotBlank(message = "O campo nome deve ser preenchido")
//    @Column(unique = true)
//    private String nomeInvestimento;
//
//    @NotNull(message = "Taxa não pode ser nula")
//    @Digits(integer = 12, fraction = 2, message = "Taxa deve ter 2 casas decimais")
//    private double taxaRendimento;
//
//    public Investimento() {
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getNomeInvestimento() {
//        return nomeInvestimento;
//    }
//
//    public void setNomeInvestimento(String nomeInvestimento) {
//        this.nomeInvestimento = nomeInvestimento;
//    }
//
//    public double getTaxaRendimento() {
//        return taxaRendimento;
//    }
//
//    public void setTaxaRendimento(double taxaRendimento) {
//        this.taxaRendimento = taxaRendimento;
//    }
//}