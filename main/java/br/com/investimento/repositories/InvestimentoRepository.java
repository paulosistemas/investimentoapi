package br.com.investimento.repositories;

import br.com.investimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer>{

//    Iterable<Investimento> findAllByNome(String nome);
//    public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
//        Iterable<Produto> findAllByNome(String nome);
//        Iterable<Produto> findAllByNomeContains(String nome);
//        Iterable<Produto> findAllByNomeAndPrecoContains(String nome, double preco);

}
