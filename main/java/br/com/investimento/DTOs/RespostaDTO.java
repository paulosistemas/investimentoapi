package br.com.investimento.DTOs;

public class RespostaDTO {

    private double rendimentoPorMes;
    private double montante;

    public RespostaDTO() {
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }
}


//public class RespostaDTO {
//    private int resultado;
//
//    public RespostaDTO() {
//    }
//
//    public int getResultado() {
//        return resultado;
//    }
//
//    public void setResultado(int resultado) {
//        this.resultado = resultado;
//    }
//}
